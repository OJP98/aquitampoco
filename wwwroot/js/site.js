﻿// Write your JavaScript code.
var control = false;

function borrarTexto() {
    document.getElementById("texto").value = "";
    alert('Mensaje enviado');
}

function verificarDispositivo() {

    var dispositivo = 'Desktop';

    if (/Android|webOS|iPhone|IEMobile|OperaMini/i.test(navigator.userAgent)) {
        dispositivo = 'Phone';

    } else if (/iPad/i.test(navigator.userAgent)) {
        dispositivo = 'Tablet'

    }

    var SO = navigator.platform;

    if (control == false) {
        document.getElementById("dispositivo").style.opacity = "1";
        alert('The device being used is: ' + dispositivo + '\nThe platform is: ' + SO);
        control = true;

    } else if (control == true) {
        document.getElementById("dispositivo").style.opacity = "0";
        control = false;
    }

    document.getElementById("respuesta").value = "The device being used is: " + dispositivo;

    return dispositivo;
}